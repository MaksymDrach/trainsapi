﻿using System.Collections.Generic;

namespace TrainsAPI.Models
{
    public class TrainModel
    {
        public int id { get; set; }
        public int number { get; set; }
        public List<string> destinations { get; set; }
    }
}