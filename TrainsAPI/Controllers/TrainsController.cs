﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrainsAPI.Models;

namespace TrainsAPI.Controllers
{
    public class TrainsController : ApiController
    {
        List<TrainModel> trains = new List<TrainModel>();

        public TrainsController()
        {
            trains.Add(new TrainModel { id = 1, number = 12, destinations = new List<string>() { "Kiev", "Lviv" } });
            trains.Add(new TrainModel { id = 2, number = 13, destinations = new List<string>() { "Kiev", "Odessa" } });
            trains.Add(new TrainModel { id = 3, number = 14, destinations = new List<string>() { "Lviv", "Odessa" } });
            trains.Add(new TrainModel { id = 4, number = 15, destinations = new List<string>() { "Poznan", "Warsaw" } });
            trains.Add(new TrainModel { id = 5, number = 16, destinations = new List<string>() { "Poznan", "Berlin" } });
        }

        public HttpResponseMessage GetAll()
        {
            return Request.CreateResponse(HttpStatusCode.OK, trains);
        }

        public HttpResponseMessage GetById(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, trains.Where(trains => trains.id == id));
        }

        public HttpResponseMessage GetByNumber(int number)
        {
            return Request.CreateResponse(HttpStatusCode.OK, trains.Where(trains => trains.number == number));
        }

        public HttpResponseMessage GetByDestinations([FromUri] string[] destinations)
        {
            return Request.CreateResponse(HttpStatusCode.OK, trains.Where(trains => trains.destinations.All(d => destinations.Contains(d))));
        }
    }
}
